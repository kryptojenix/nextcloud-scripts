#!/bin/bash
# /usr/local/bin/nextcloud/setpubip.sh

# modify the following variables according to your installation
path=/var/www/nextcloud/config/
file=config.php
static="192.168.0.99"

#echo $path
#echo $file
#echo $static


#backup the config.php file
cp $path$file $path$file.old


# the string to search for in the config file 
# then use it to construct the replacement string

string="  0 => '$static', "
#echo $string

update_static_ip(){

  # edit the config.php file. Replace the old line with the new one
  # note: this replaces every address after the static IP defined in this file
  sed -i "/0 => /c\\$string" $path$file
  echo New IP has been configured

  # copy the template and append the new IP
  cp msg_template.txt new_msg.txt
  echo $pubip >> new_msg.txt  

  # send myself an email so that I know that the IP has changed
  ssmtp example@test.com < new_msg.txt

}


# read the line from current config file
ipline=`cat $path$file |grep "0 =>"`


# use this website to return my public IP address 
# it is from this tutorial https://ccm.net/faq/1866-finding-your-public-ip-address-in-shell-script
pubip=`GET www.monip.org | sed -nre 's/^.* (([0-9]{1,3}\.){3}[0-9]{1,3}).*$/\1/p'`

# construct the config line for comparison
string+="'$pubip'"
echo Old config $ipline
echo New config $string

if [ "$string" == "$ipline" ]
then
  echo IP has not changed
else
  echo New IP detected
  update_static_ip
fi

