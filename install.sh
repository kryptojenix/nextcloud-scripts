#!/bin/bash
sudo cp setpubip.sh /usr/local/bin/nextcloud
sudo chmod 744 /usr/local/bin/nextcloud/setpubip.sh

# check crontab for a reference to this file
# TODO: this

# add the line to /etc/crontab
# TODO: this

# if that failed
echo Now add this script to /etc/crontab with the following line

echo "10 *    * * *   root    /usr/local/bin/nextcloud/setpubip.sh"
echo Done.
